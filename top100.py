import requests

url_dict = {
    '全站': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=0&type=all',
    # '番剧': 'https://api.bilibili.com/pgc/web/rank/list?day=3&season_type=1',  #
    # '国产动画': 'https://api.bilibili.com/pgc/season/rank/web/list?day=3&season_type=4',  #
    # '国创相关': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=168&type=all',  #
    # '纪录片': 'https://api.bilibili.com/pgc/season/rank/web/list?day=3&season_type=3',  #
    '动画': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=1&type=all',
    '音乐': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=3&type=all',
    '舞蹈': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=129&type=all',
    '游戏': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=4&type=all',
    '知识': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=36&type=all',
    '科技': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=188&type=all',
    '运动': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=234&type=all',
    '汽车': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=223&type=all',
    '生活': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=160&type=all',
    '美食': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=211&type=all',
    '动物圈': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=217&type=all',
    '鬼畜': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=119&type=all',
    '时尚': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=155&type=all',
    '娱乐': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=5&type=all',
    '影视': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=181&type=all',
    # # '电影': 'https://api.bilibili.com/pgc/season/rank/web/list?day=3&season_type=2',  #
    # # '电视剧': 'https://api.bilibili.com/pgc/season/rank/web/list?day=3&season_type=5',  #
    '原创': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=0&type=origin',
    '新人': 'https://api.bilibili.com/x/web-interface/ranking/v2?rid=0&type=rookie',
}


def get_data(url):
    r = requests.get(url)
    print(r.status_code)
    json_data = r.json()
    list_data = json_data['data']['list']
    title_list = []
    pic_list = []
    play_cnt_list = []
    danmu_cnt_list = []
    coin_cnt_list = []
    like_cnt_list = []
    dislike_cnt_list = []
    share_cnt_list = []
    favorite_cnt_list = []
    author_list = []
    score_list = []
    video_url = []
    for data in list_data:
        title_list.append(data['title'])  # 视频标题
        pic_list.append(data['pic'])  # 封面
        play_cnt_list.append(data['stat']['view'])  # 播放数
        danmu_cnt_list.append(data['stat']['danmaku'])  # 弹幕数
        coin_cnt_list.append(data['stat']['coin'])  # 投币数
        like_cnt_list.append(data['stat']['like'])  # 点赞数
        dislike_cnt_list.append(data['stat']['dislike'])  # 点踩数
        share_cnt_list.append(data['stat']['share'])  # 分享数
        favorite_cnt_list.append(data['stat']['favorite'])  # 收藏数
        author_list.append(data['owner']['name'])  # 作者
        score_list.append(data['score'])  # 评分
        video_url.append('https://www.bilibili.com/video/' + data['bvid'])  # 视频链接
    # get_img_all(pic_list)


def get_img_all(img_list):
    # 定义变量x并初始化用来计数图片的张数
    x = 0
    # 遍历
    for imgurl in img_list:
        # 获取获得的从imglist中遍历得到的imgurl
        imgres = requests.get(imgurl)
        with open("img\{}.jpg".format(x), "wb") as f:
            f.write(imgres.content)
            x += 1
            print("第", x, "张")
    print("下载完毕")


get_data(url_dict["全站"])
